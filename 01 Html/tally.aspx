﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="tally.aspx.cs" Inherits="matrakom.tally"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="Tally" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Tally ERP Accounting</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h2>
                                                        Tally ERP Accounting</h2>
                                                    <a href="http://www.tallysolutions.com">
                                                        <img class="img-indent alt" alt="" src="images/tallyc.jpg" /></a>
                                                    <p>
                                                        One of world’s best proven windows based accounting and inventory application from
                                                        India since 1996. Our consultants have implemented Tally in over 500 companies in
                                                        Indonesia alone, while Tally is used by over million users worldwide. We are capable
                                                        of not only implementation of the product, but also associated services as data
                                                        migration, developing bridge programs for data transfers and other customizations.
                                                        Our Tally consultants have been associated with the product for more than a decade.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <div class="wrapper">
            <div class="col-1 maxheight">
                <!-- box1 begin -->
                <div class="box">
                    <div class="border-top">
                        <div class="border-right">
                            <div class="border-bot">
                                <div class="border-left">
                                    <div class="left-top-corner maxheight">
                                        <div class="right-top-corner maxheight">
                                            <div class="right-bot-corner maxheight">
                                                <div class="left-bot-corner maxheight">
                                                    <div class="inner">
                                                        <h3>
                                                            Some of our clients</h3>
                                                        <p>
                                                            <img src="images/tallyc1.jpg" width="544" height="372" alt="enta clients" /><br />
                                                            <!--<p class="aligncenter"><img alt="" src="images/price1.jpg" /></p> -->
                                                        </p>
                                                        <p>
                                                            &nbsp;</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- box1 end -->
            </div>
            <div class="col-3 maxheight">
                <!-- box1 begin -->
                <div class="box">
                    <div class="border-top">
                        <div class="border-right">
                            <div class="border-bot">
                                <div class="border-left">
                                    <div class="inner">
                                        <h3>
                                            Our Clients</h3>
                                        <p>
                                            These companies has using ENTA to help them improve</p>
                                        <div id="news-container">
                                            <ul class="list3 color2">
                                                <li><span><strong>PT. Adani Global</strong></span></li>
                                                <li><span><strong>PT. Octovate Group Asia Goldindo Perkasa</strong></span></li>
                                                <li><span><strong>PT(Star Alliance Intimates Semarang)</strong></span></li>
                                                <li><span><strong>PT. Red Tree Indonesia (Octovate Group Asia) SAI Apparels</strong></span></li>
                                                <li><span><strong>PT. Media Kreasi Komunika (FIRST POSITION) </strong></span></li>
                                                <li><span><strong>PT. Tainan Enterprises Indonesia</strong></span></li>
                                                <li><span><strong>PT. Cabe Rawit Pariwara </strong></span></li>
                                                <li><span><strong>PT. Triveni Export</strong></span></li>
                                                <li><span><strong>PT. Duppont Indonesia </strong></span></li>
                                                <li><span><strong>PT. Sainath Industries (Jakarta & Semarang)</strong></span></li>
                                                <li><span><strong>PT. Minda Asean Automotive- Karawang </strong></span></li>
                                                <li><span><strong>PT. Saraswati Garmindo</strong></span></li>
                                                <li><span><strong>PT. Mores Rowland </strong></span></li>
                                                <li><span><strong>PT. Kedaung Group</strong></span></li>
                                                <li><span><strong>PT. Aje Indonesia </strong></span></li>
                                                <li><span><strong>PT. Bali Permai</strong></span></li>
                                                <li><span><strong>PT. Bohler Welding Group South East Asia </strong></span></li>
                                                <li><span><strong>PT. Galib Instalindo Utama (Vermeer)</strong></span></li>
                                                <li><span><strong>PT. Energi Canggih Indonesia </strong></span></li>
                                                <li><span><strong>RS St. Borormeus -Bandung</strong></span></li>
                                                <li><span><strong>PT Johnson Mathey Ceramics </strong></span></li>
                                                <li><span><strong>PT. United Insurance Services</strong></span></li>
                                                <li><span><strong>PT. Wira Agri Sejati- Padang </strong></span></li>
                                                <li><span><strong>PT. Teamworx Indonesia</strong></span></li>
                                                <li><span><strong>PT. Mulya Benyamin Paramitha </strong></span></li>
                                                <li><span><strong>PT Surya Esa Perkasa-Jakarta Palembang</strong></span></li>
                                                <li><span><strong>PT Core Mineral </strong></span></li>
                                                <li><span><strong>PT. Jaykay Files Indonesia- Surabaya</strong></span></li>
                                                <li><span><strong>PT. Madhucon Indonesia </strong></span></li>
                                                <li><span><strong>PT. Sundaya Indonesia</strong></span></li>
                                                <li><span><strong>PT. Megatek Komsindo</strong></span></li>
                                                <li><span><strong>PT. Chertechs</strong></span></li>
                                                <li><span><strong>PT. Emerio Corp Indonesia </strong></span></li>
                                                <li><span><strong>PT. Federal Food Internusa</strong></span></li>
                                                <li><span><strong>PT. Mandaya Service Container </strong></span></li>
                                                <li><span><strong>PT. Olam Indonesia</strong></span></li>
                                                <li><span><strong>PT. Balfour Beatty Sakti Indonesia </strong></span></li>
                                                <li><span><strong>PT. Lintas Surya Alam Industries</strong></span></li>
                                                <li><span><strong>PT. Mandawani Mandiri </strong></span></li>
                                                <li><span><strong>PT. Sari Wangi AEA</strong></span></li>
                                                <li><span><strong>Mountview Int. Christain School Salatiga </strong></span></li>
                                                <li><span><strong>PT. Solindo Grapika - Solo</strong></span></li>
                                                <li><span><strong>Puska UI - Impact Project </strong></span></li>
                                                <li><span><strong>PT Sandhar Indonesia</strong></span></li>
                                                <li><span><strong>Medan International School </strong></span></li>
                                                <li><span><strong>PT. Core Mineral Indonesia</strong></span></li>
                                                <li><span><strong>Yayasan Pendidikan Asian Pasifik (Singapore International School -
                                                    Bonavista ) </strong></span></li>
                                                <li><span><strong>PT. Topasli Snack</strong></span></li>
                                                <li><span><strong>PT. Bina Artha </strong></span></li>
                                                <li><span><strong>PT. Global Indonesia Food</strong></span></li>
                                                <li><span><strong>PT Galang Kreasi Pusakajaya </strong></span></li>
                                                <li><span><strong>PT. Global Ingredients Flavors’</strong></span></li>
                                                <li><span><strong>PT. Wigas Santana </strong></span></li>
                                                <li><span><strong>PT. Tirta Artha Jaya</strong></span></li>
                                                <li><span><strong>PT. Ausdoc Geoservices Indonesia </strong></span></li>
                                                <li><span><strong>PT. Colorpack Indonesia</strong></span></li>
                                                <li><span><strong>PT. Servo Fire Indonesia </strong></span></li>
                                                <li><span><strong>PT. Unilever Indonesia</strong></span></li>
                                                <li><span><strong>PT. Aroma Gourmet ( Penang Bistro ) </strong></span></li>
                                                <li><span><strong>PT. Flowerserve</strong></span></li>
                                                <li><span><strong>PT. Aneka Jaya Ikan </strong></span></li>
                                                <li><span><strong>PT. Sedap Wangi</strong></span></li>
                                                <li><span><strong>PT. Sree International Indonesia </strong></span></li>
                                                <li><span><strong>PT. Rimba Kencana Bumi Nusa</strong></span></li>
                                                <li><span><strong>PT. Embee Plumbon Tekstil- Cirebon </strong></span></li>
                                                <li><span><strong>PT. Daya Esa Mulya mandiri</strong></span></li>
                                                <li><span><strong>PT. Tainan Enterprise </strong></span></li>
                                                <li><span><strong>PT. Sibelco Lautan Mineral</strong></span></li>
                                                <li><span><strong>PT. Busana Remaja Textiles Group </strong></span></li>
                                                <li><span><strong>PT. Mineral General Resources</strong></span></li>
                                                <li><span><strong>PT. Garuda Mas Semesta- Bandung </strong></span></li>
                                                <li><span><strong>PT. Adani Global- Jakarta &Kalimantan Bunyu</strong></span></li>
                                                <li><span><strong>PT. Indorama Synthetics </strong></span></li>
                                                <li><span><strong>PT. Geoservices Indonesia</strong></span></li>
                                                <li><span><strong>PT SC Enterprises Indonesia </strong></span></li>
                                                <li><span><strong>PT. Coal & Oil Indonesia</strong></span></li>
                                                <li><span><strong>PT. Bintang Batubara Indonesia </strong></span></li>
                                                <li><span><strong>PT. Surya Esa Perkasa </strong></span></li>
                                            </ul>
                                        </div>
                                        <p>
                                            To get more detail or enquiry please send us an email</p>
                                        <!--<p class="aligncenter"><img alt="" src="images/price1.jpg" /></p> -->
                                        <a href="#" class="button"><em><b>Contact us for enquiry</b></em></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- box1 end -->
            </div>
        </div>
    </div>
</asp:Content>
