﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="enta.aspx.cs" Inherits="matrakom.enta"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="Enta" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    ENTA Application For Human Resource</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h2>
                                                        ENTA-HR (Payroll and personnel management system)</h2>
                                                    This is sold in the Indonesian market as a product and has installations in about
                                                    50 companies, most of them large factories. The system covers all aspects of payroll
                                                    processing including personnel management, time management, allowances and deductions,
                                                    compensations, leave management, loan management, reimbursements and tax management
                                                    for employees. This can be interfaced with the Attendance machine, bank transfer/payment
                                                    gateways etc. This is also linked to a number of Financial Accounting modules as
                                                    AccPac, SUN system, SAP and others.</div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <div class="wrapper">
            <div class="col-1 maxheight">
                <!-- box1 begin -->
                <div class="box">
                    <div class="border-top">
                        <div class="border-right">
                            <div class="border-bot">
                                <div class="border-left">
                                    <div class="left-top-corner maxheight">
                                        <div class="right-top-corner maxheight">
                                            <div class="right-bot-corner maxheight">
                                                <div class="left-bot-corner maxheight">
                                                    <div class="inner">
                                                        <h3>
                                                            Some of our clients</h3>
                                                        <p>
                                                            <img src="images/entac.jpg" width="544" height="372" alt="enta clients" /><br />
                                                            <!--<p class="aligncenter"><img alt="" src="images/price1.jpg" /></p> -->
                                                        </p>
                                                        <p>
                                                            &nbsp;</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- box1 end -->
            </div>
            <div class="col-3 maxheight">
                <!-- box1 begin -->
                <div class="box">
                    <div class="border-top">
                        <div class="border-right">
                            <div class="border-bot">
                                <div class="border-left">
                                    <div class="left-top-corner maxheight">
                                        <div class="right-top-corner maxheight">
                                            <div class="right-bot-corner maxheight">
                                                <div class="left-bot-corner maxheight">
                                                    <div class="inner">
                                                        <h3>
                                                            Our Clients</h3>
                                                        <p>
                                                            These companies has using ENTA to help them improve</p>
                                                        <div id="news-container">
                                                            <ul class="list3 color2">
                                                                <li><span><strong>PT. Adani Global</strong></span></li>
                                                                <li><span><strong>PT. Sibelco Lautan Minerals</strong></span></li>
                                                                <li><span><strong>PT. RSM AAJ Associates</strong></span></li>
                                                                <li><span><strong>Bank SBI Indonesia (SBI)</strong></span></li>
                                                                <li><span><strong>PT. Core mineral Indonesia</strong></span></li>
                                                                <li><span><strong>PT. Sumber Mitra Jaya</strong></span></li>
                                                                <li><span><strong>PT. Saeti Concretindo Wahana</strong></span></li>
                                                                <li><span><strong>PT. Emerio Indonesia</strong></span></li>
                                                                <li><span><strong>PT. Balfour Beaty Sakti </strong></span></li>
                                                                <li><span><strong>PT. GTL Indonesia</strong></span></li>
                                                                <li><span><strong>PT. Teamworx Indonesia </strong></span></li>
                                                                <li><span><strong>PT. Dharmapala Usaha Sukses</strong></span></li>
                                                                <li><span><strong>PT. KSB Indonesia </strong></span></li>
                                                                <li><span><strong>PT. Esabindo Pratama</strong></span></li>
                                                                <li><span><strong>Singapore International School</strong></span></li>
                                                                <li><span><strong>PT. Karya Yasantara Cakti</strong></span></li>
                                                                <li><span><strong>PT. Malindo Feedmill Indonesia </strong></span></li>
                                                                <li><span><strong>PT. Faber Castell Indonesia</strong></span></li>
                                                                <li><span><strong>PT. Busana Remaja Group </strong></span></li>
                                                                <li><span><strong>PT. Jayanata Kosmetika Prima</strong></span></li>
                                                                <li><span><strong>PT. Sodexho Pass Indonesia </strong></span></li>
                                                                <li><span><strong>PT. Karya Yasantara Cakti</strong></span></li>
                                                                <li><span><strong>PT. Goldindo Perkasa</strong></span></li>
                                                                <li><span><strong>PT Star Alliance Intimates Semarang, PT)</strong></span></li>
                                                                <li><span><strong>PT. Jasa Medivest</strong></span></li>
                                                                <li><span><strong>PT. Asih Eka Abadi (SOS Hospoial)</strong></span></li>
                                                                <li><span><strong>PT. Sandhar Indonesia </strong></span></li>
                                                                <li><span><strong>PT. Wintermar</strong></span></li>
                                                                <li><span><strong>PT. Penta Laju Jaya Motor Loncin Motor </strong></span></li>
                                                                <li><span><strong>PT. Rama Perwira (Grey World Wide, PT)</strong></span></li>
                                                                <li><span><strong>PT. Sundaya Indonesia </strong></span></li>
                                                                <li><span><strong>PT. Avery Dennison</strong></span></li>
                                                                <li><span><strong>PT. Mutiara Biru Perkasa </strong></span></li>
                                                                <li><span><strong>PT. Polymindo Permata</strong></span></li>
                                                            </ul>
                                                        </div>
                                                        <p>
                                                            To get more detail or enquiry please send us an email</p>
                                                        <!--<p class="aligncenter"><img alt="" src="images/price1.jpg" /></p> -->
                                                        <a href="#" class="button"><em><b>Contact us for enquiry</b></em></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- box1 end -->
            </div>
        </div>
    </div>
</asp:Content>
