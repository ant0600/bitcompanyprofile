﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility;

namespace matrakom
{
    public partial class about_m : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            IMasterPage masterPage = Master as IMasterPage;
            if (masterPage != null)
            {
                masterPage.BodyId = "page2";
            }

        }
    }
}
