﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Utility;

namespace matrakom
{
    public partial class DefaultMaster : System.Web.UI.MasterPage, IMasterPage
    {
        private string _bodyId;

        public string BodyId
        {
            get { return _bodyId; }
            set { _bodyId = value; }
        }

        private string _bodyClass;

        public string BodyClass
        {
            get { return _bodyClass; }
            set { _bodyClass = value; }
        }

       
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }
    }
}
