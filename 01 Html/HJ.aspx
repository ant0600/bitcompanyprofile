﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="HJ.aspx.cs" Inherits="matrakom.HJ"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="Service" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Hot Jobs</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h2>
                                                        Hot Jobs</h2>
                                                    <h3>
                                                        List of Available Position</h3>
                                                    <!--<p><strong>Our Products and Services- Solution</strong>, Quality, and Price are three key elements that affect corporate decisions. It is precisely these three characteristics that give our products and services the leading edge. The management and consultants at Matrkom possess years of experience in providing software and consulting services for the Indonesian market and are well versed in selling, implementing and maintaining software applications.<br />
                            While the company’s marketing and sales office is located in Indonesia, we have established our own software development centre in India, which is known as world’s IT ‘kitchen’.<br />
  <br />
                          Our team has been associated with ‘Tally’, one of world’s best proven windows based accounting and inventory application from India since 1996. Our consultants have implemented Tally in over 500 companies in Indonesia while Tally is used by over million users worldwide. We are capable of not only implementation, but also other associated services as data migration, bridge program for data transfers and other customizations. Our Tally division has two consultants who have been associated with the product for more than a decade.</p>
                          <p><strong>Our service target</strong>
                            <br />
                            Focus on small to  medium enterprises for Tally, ENTA, ATAMS <br />
                            Focus on large corporate entities for  Business Analytics solutions
						    <br />
					      Develop a reseller model through an  extensive network of partners in major Indonesian cities</p></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- box end -->
            </div>
            <!-- box begin
    <div class="box">
      <div class="border-top">
        <div class="border-right">
          <div class="border-bot">
            <div class="border-left">
              <div class="left-top-corner">
                <div class="right-top-corner">
                  <div class="right-bot-corner">
                    <div class="left-bot-corner">
                      <div class="inner">
                        <h2>Our Team</h2>
                        <ul class="list2">
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>John Doe</strong> President and a big boss</h4>
                            The presidents always come first, but we’re sure you knew that. So here it is - the info about the biggest man on your company. </li>
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Sam Cohen</strong> Vice president</h4>
                            Also some kind of info about how good he is at whatever he does - that’s what vice presidents are for. </li>
                          <li class="last"> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Jane Doe</strong> Personal assistant</h4>
                            This nice lady would love if you say a couple of good words about her on this page. Besides, it’s not a big deal for you - than why don’t you be nice and just put the info. </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
            <!-- box end -->
        </div>
    </div>
</asp:Content>
