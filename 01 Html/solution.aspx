﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="solution.aspx.cs" Inherits="matrakom.solution"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="Solution" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Asset Management</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h3>
                                                        ATAMS</h3>
                                                    <ul class="list2">
                                                        <li>
                                                            <img alt="" src="Images/atamsmall.jpg" />
                                                            <h4>
                                                                <strong><a name="atams" class="list2" id="atams">ATAMS- Asset Tracking and Maintenance
                                                                    System</a></strong></h4>
                                                            <p>
                                                                This is a software suit designed to track the Assets of any company and set and
                                                                manage maintenance schedule. This software is very useful for factories which have
                                                                deployed large machineries, mining sites, properties and other operational sites
                                                                where various types of equipments are deployed at multiple locations. ATAMS is a
                                                                web based application developed to take care of the Assets and maintenance of any
                                                                large organization, where there are multiple types of assets, components and spare
                                                                parts are deployed at various locations.
                                                            </p>
                                                        </li>
                                                        <li>ATAMS consists of mainly three modules, Inventory, Maintenance and Personnel for
                                                            the engineering staff. The main benefits of ATAMS user is that the product is customizable
                                                            to the requirement of the users since we own our product. The web based architecture
                                                            allows easy installation, maintenance and offers connectivity to the HO. ATAMS is
                                                            built with a number of alert functions to alert the management of overdue maintenance
                                                            tasks left undone, any approvals overdue etc. The alerts will indicate the geographical
                                                            location of the asset for easy access for the repair engineer </p></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
    </div>
</asp:Content>
