<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="matrakom.Default"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="Content" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-big begin -->
        <div id="jheader">
            <div class="wrap">
                <div id="slide-holder">
                    <div id="slide-runner">
                        <a href="">
                            <img id="slide-img-1" src="images/products.png" class="slide" alt="" /></a>
                        <a href="">
                            <img id="slide-img-2" src="images/atams.png" class="slide" alt="" /></a> <a href="">
                                <img id="slide-img-3" src="images/prTally.png" class="slide" alt="" /></a>
                        <!--<a href=""><img id="slide-img-4" src="images/prTally.png" class="slide" alt="" /></a>-->
                        <div id="slide-controls">
                            <p id="slide-client" class="text">
                                <strong>Product: </strong><span></span>
                            </p>
                            <p id="slide-desc" class="text">
                            </p>
                            <p id="slide-nav">
                            </p>
                        </div>
                    </div>
                </div>
                <!--content featured gallery here -->
                <script type="text/javascript">
                    if (!window.slider) var slider = {}; slider.data = [
	{ "id": "slide-img-1", "client": "ENTA HR", "desc": "pt. Matrakom Product" },
	{ "id": "slide-img-2", "client": "ATAMS", "desc": "pt. Matrakom Product" },
	{ "id": "slide-img-3", "client": "TALLY ERP 9", "desc": "pt. Matrakom Product"}];
                    /*{"id":"slide-img-4","client":"nature beauty","desc":"add your description here"},
                    {"id":"slide-img-5","client":"nature beauty","desc":"add your description here"},
                    {"id":"slide-img-6","client":"nature beauty","desc":"add your description here"},
                    {"id":"slide-img-7","client":"nature beauty","desc":"add your description here"}*/
                </script>
            </div>
        </div>
        <!-- main-banner-big end -->
        <div class="inner">
            <!-- main-banner-small end -->
            <div class="section">
                <!-- box begin -->
                <div class="box">
                    <div class="border-top">
                        <div class="border-right">
                            <div class="border-bot">
                                <div class="border-left">
                                    <div class="left-top-corner maxheight">
                                        <div class="right-top-corner maxheight">
                                            <div class="right-bot-corner maxheight">
                                                <div class="left-bot-corner maxheight">
                                                    <div class="inner">
                                                        <h3>
                                                            Welcome to Our Site</h3>
                                                        <div id="textContainer">
                                                            <div class="floatLeft">
                                                                <div id="accordion">
                                                                    <h2 class="current">
                                                                        Business Intelligence Technology</h2>
                                                                    <div class="pane" style="display: block">
                                                                        <p>
                                                                            <img src="Images/Logo BIT 2.png" width="142" height="83" /></p>
                                                                        <h3>
                                                                            Overview</h3>
                                                                        <p>
                                                                            Bizintek (Business Inteligent Tecnology) is a dynamic and well established software
                                                                            development service company with substantial experience in providing consulting
                                                                            services for IT and Telecomunications sectoce in Indonesia since 2002. BIT has Establish
                                                                            subsidiaries to enter new business areas (since 2009. Our group company consist
                                                                            of pt. Matrakom Infotek and pt. Maya International
                                                                        </p>
                                                                        <div class="news_details">
                                                                            <span class="date">Read</span> <span class="user">more</span></div>
                                                                    </div>
                                                                    <h2>
                                                                        Matrakom Infotek</h2>
                                                                    <div class="pane">
                                                                        <img src="Images/icon1.gif" alt="JavaScript tools" style="float: left; margin: 0 15px 15px 0" />
                                                                        <h3>
                                                                            Overview</h3>
                                                                        <p>
                                                                            Matrkom possess years of experience in providing software and consulting services
                                                                            for the Indonesian market and are well versed in selling, implementing and maintaining
                                                                            software applications.
                                                                        </p>
                                                                        <p style="clear: both">
                                                                            &nbsp;
                                                                        </p>
                                                                        <div class="news_details">
                                                                            <a href="about-m.aspx"><span class="date">Read</span> <span class="user">more</span></a></div>
                                                                    </div>
                                                                    <h2>
                                                                        Maya International</h2>
                                                                    <div class="pane">
                                                                        <img src="Images/palet02.png" alt="JavaScript tools" style="float: left; margin: 0 15px 15px 0" />
                                                                        <h3>
                                                                            Overview</h3>
                                                                        <p>
                                                                            We are general trade company in industrial goods which 100 % owned by BIT since
                                                                            2010. Our goal is to develop dealer and service network for industrial products
                                                                        </p>
                                                                        <p style="clear: both">
                                                                            &nbsp;
                                                                        </p>
                                                                        <div class="news_details">
                                                                            <a href="about-maya.aspx"><span class="date">Read</span> <span class="user">more</span></a></div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="floatRight">
                                                                <!--<div class="title">Latest News</div>
				                    <div class="news_tab">
                                    <h3>Job Requirement</h3>
                                    <img src="Images/report.png" alt="JavaScript tools" style="float:left; margin:0 15px 15px 0" />
                                    <p>
                    "Open Recruitmen Jobs for Under and Post Graduate to become IT Soluttion"</p>
                                    <div class="news_details"><span class="date">Read</span> <span class="user">more</span></div>
                                    </div>

                      <div class="news_tab">
                                    <h3>Picture for Bizintek</h3>
                                <img src="Images/report.png" alt="JavaScript tools" style="float:left; margin:0 15px 15px 0" />
                                    <p>
                    "Still Under Cosnstruction."</p>
                                    <p>&nbsp;</p>
                                  <div class="news_details"><span class="date">Read</span> <span class="user">more</span></div>
                                    </div>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- box end -->
            </div>
        </div>
    </div>
</asp:Content>
