﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="about-maya.aspx.cs" Inherits="matrakom.about_maya"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="AboutMaya" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Maya International Indonesia</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h3>
                                                        About pt Maya International Indonesia</h3>
                                                    <p>
                                                        Is Importing and trading company in industrial goods which 100 % owned by BIT since
                                                        2010.<br />
                                                        Our goal is to develop dealer and service network for industrial products.</p>
                                                    <p>
                                                        Other details is waiting..........</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h3>
                                                        Our Vision</h3>
                                                    <p>
                                                        Is to be a leading, innovative IT company that delivers high quality IT systems
                                                        and services against competitive costs. Other detail is waiting....</p>
                                                    <h3>
                                                        Our Mission</h3>
                                                    <p>
                                                        Introducing new and advanced antennae solutions for telecommunications companies(Maya).Information
                                                        can be retrieved quickly and accurately,anywhere and anytime in the world. Straight-through
                                                        processing (STP) replaces your legacy systems and problems. Other detail is waiting....</p>
                                                    <h3>
                                                        Our Product and Services</h3>
                                                    <p>
                                                        Solution, Quality, and Price are three key elements that affect corporate decisions.
                                                        It is precisely these three characteristics that give our products and services
                                                        the leading edge. The management and consultants at Matrkom possess years of experience
                                                        in providing software and consulting services for the Indonesian market and are
                                                        well versed in selling, implementing and maintaining software applications. While
                                                        the company’s marketing and sales office is located in Indonesia, we have established
                                                        our own software development centre in India, which is known as world’s IT ‘kitchen’.
                                                        Other detail is waiting....<br />
                                                        <br />
                                                        Our team has been associated with ‘Tally’, one of world’s best proven windows based
                                                        accounting and inventory application from India since 1996. Our consultants have
                                                        implemented Tally in over 500 companies in Indonesia while Tally is used by over
                                                        million users worldwide. We are capable of not only implementation, but also other
                                                        associated services as data migration, bridge program for data transfers and other
                                                        customizations. Our Tally division has two consultants who have been associated
                                                        with the product for more than a decade. Other detail is waiting....</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <!-- box begin
    <div class="box">
      <div class="border-top">
        <div class="border-right">
          <div class="border-bot">
            <div class="border-left">
              <div class="left-top-corner">
                <div class="right-top-corner">
                  <div class="right-bot-corner">
                    <div class="left-bot-corner">
                      <div class="inner">
                        <h2>Our Team</h2>
                        <ul class="list2">
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>John Doe</strong> President and a big boss</h4>
                            The presidents always come first, but we’re sure you knew that. So here it is - the info about the biggest man on your company. </li>
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Sam Cohen</strong> Vice president</h4>
                            Also some kind of info about how good he is at whatever he does - that’s what vice presidents are for. </li>
                          <li class="last"> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Jane Doe</strong> Personal assistant</h4>
                            This nice lady would love if you say a couple of good words about her on this page. Besides, it’s not a big deal for you - than why don’t you be nice and just put the info. </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    </div> -->
        <!-- box end -->
    </div>
    <!-- extra-content -->
    <div id="extra-content">
        <div class="container">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner">
                                    <div class="right-top-corner">
                                        <div class="right-bot-corner">
                                            <div class="left-bot-corner">
                                                <div class="inner">
                                                    <h3>
                                                        Partners...</h3>
                                                    <div class="carousel">
                                                        <ul>
                                                            <!--<li class="prev"><img alt="" src="images/prev.gif" /></li> -->
                                                            <li></li>
                                                            <li></li>
                                                            <li></li>
                                                            <li></li>
                                                            <!--<li class="next"><img alt="" src="images/next.gif" /></li>-->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
    </div>
</asp:Content>
