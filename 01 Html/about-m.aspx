﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="about-m.aspx.cs" Inherits="matrakom.about_m"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="AboutM" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Matrakom Infotek</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h3>
                                                        About Matrakom</h3>
                                                    <p>
                                                        Matrakom has been established as a software and service provider in Indonesia for
                                                        15 years. We are master Tally Partner In Indoesia, also we owns IPR for ENTA - a
                                                        HR and Payrole system specifically develop for Indonesian market. We also own IPR
                                                        for ATAMS - an Asset Tracking and Maintenance Managemen System.<br />
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h3>
                                                        Our Vision</h3>
                                                    <p>
                                                        Is to be a leading, innovative IT company that delivers high quality IT systems
                                                        and services against competitive costs for Indonesian Market.</p>
                                                    <h3>
                                                        Our Mission</h3>
                                                    <p>
                                                        Is to develop software systems that are effective, simple, reliable, inexpensive
                                                        and fully integrated. Information can be retrieved quickly and accurately,anywhere
                                                        and anytime in the world. Straight-through processing (STP) replaces your legacy
                                                        systems and problems.</p>
                                                    <h3>
                                                        Our Product and Services</h3>
                                                    <p>
                                                        Solution, Quality, and Price are three key elements that affect corporate decisions.
                                                        It is precisely these three characteristics that give our products and services
                                                        the leading edge. The management and consultants at Matrkom possess years of experience
                                                        in providing software and consulting services for the Indonesian market and are
                                                        well versed in selling, implementing and maintaining software applications.
                                                        <br />
                                                        <br />
                                                        Our service and products is focusing on Indonesian market, targeting on small to
                                                        medium enterprises for Tally ERP accounting, HR and Payrole solution (ENTAHR) and
                                                        Asset Tracking and Maintenance System solution (ATAMS). In order to delivery Quality
                                                        of Service we always strengthen and developt our support capabilities on a continuous
                                                        basis.</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <!-- box begin
    <div class="box">
      <div class="border-top">
        <div class="border-right">
          <div class="border-bot">
            <div class="border-left">
              <div class="left-top-corner">
                <div class="right-top-corner">
                  <div class="right-bot-corner">
                    <div class="left-bot-corner">
                      <div class="inner">
                        <h2>Our Team</h2>
                        <ul class="list2">
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>John Doe</strong> President and a big boss</h4>
                            The presidents always come first, but we’re sure you knew that. So here it is - the info about the biggest man on your company. </li>
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Sam Cohen</strong> Vice president</h4>
                            Also some kind of info about how good he is at whatever he does - that’s what vice presidents are for. </li>
                          <li class="last"> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Jane Doe</strong> Personal assistant</h4>
                            This nice lady would love if you say a couple of good words about her on this page. Besides, it’s not a big deal for you - than why don’t you be nice and just put the info. </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
        
    </div> -->
        <!-- box end -->
    </div>
    <!-- extra-content -->
    <div id="extra-content">
        <div class="container">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner">
                                    <div class="right-top-corner">
                                        <div class="right-bot-corner">
                                            <div class="left-bot-corner">
                                                <div class="inner">
                                                    <h3>
                                                        Partners
                                                        <div class="carousel">
                                                            <ul>
                                                                <!--<li class="prev"><img alt="" src="images/prev.gif" /></li> -->
                                                                <li></li>
                                                                <li></li>
                                                                <li></li>
                                                                <li></li>
                                                                <!--<li class="next"><img alt="" src="images/next.gif" /></li>-->
                                                            </ul>
                                                        </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
    </div>
</asp:Content>
