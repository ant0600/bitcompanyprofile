﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="BoD.aspx.cs" Inherits="matrakom.BoD"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="Support" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Board of Directors</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h3>
                                                        Board of Directors
                                                    </h3>
                                                    <!--<p><strong>Technical competence. </strong>Our core competency is in Microsoft technologies and most of our developers are endorsed by Microsoft. Our database team has competence in SQL server as well as in Oracle. We adopted Microsoft.Net framework as the development platform and all our major projects are being developed in this platform. We adopted Object oriented architecture and it is based on a modified version of MVC (Model-View-Controller) Pattern.                          </p>
                          <p><strong>Development Methodology. </strong>We follow a framework approach to all our development projects. We have already developed frameworks for security, personal management, and request-approval. These frameworks have helped us to reduce the project lead time as well the total development effort. The application design is based on OOAD and follows the best practices using UML. For the projects with well defined user requirements we are follow the classic waterfall methodology. But for the product development we started experimenting with the Agile SCRUM methodology which ensures a constant review-feedback and improvement loop.                          </p>
                          <p><strong>Project Methodology.</strong> Our Project Management follows PMI PMBOK methodology and involves multiple major and interim milestones. We have weekly project review meetings which tracks the cost in terms of effort and resources. Each project phase has a stage gate where the deliverables are reviewed and the plan for the next phase is rolled over.<br />
                          </p>
                          <p><strong>Quality Initiatives.</strong> We started the quality initiatives last year and the major templates and processes are in place now. The QA process involves code reviews and peer reviews. The application code and related documents are kept under version control and privileges are defined based on the roles in the project. The QC process has given preference on User requirements rather than technology. The process is based on a Test plan and related test cases derived from the use cases. All the bugs from the different levels of testing are logged into the bug tracker software and taken for bug triag</p></div>-->
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- box end -->
            </div>
            <!-- box begin
    <div class="box">
      <div class="border-top">
        <div class="border-right">
          <div class="border-bot">
            <div class="border-left">
              <div class="left-top-corner">
                <div class="right-top-corner">
                  <div class="right-bot-corner">
                    <div class="left-bot-corner">
                      <div class="inner">
                        <h2>Our Team</h2>
                        <ul class="list2">e
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>John Doe</strong> President and a big boss</h4>
                            The presidents always come first, but we’re sure you knew that. So here it is - the info about the biggest man on your company. </li>
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Sam Cohen</strong> Vice president</h4>
                            Also some kind of info about how good he is at whatever he does - that’s what vice presidents are for. </li>
                          <li class="last"> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Jane Doe</strong> Personal assistant</h4>
                            This nice lady would love if you say a couple of good words about her on this page. Besides, it’s not a big deal for you - than why don’t you be nice and just put the info. </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div> -->
            <!-- box end -->
        </div>
    </div>
</asp:Content>
