﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="contact-us.aspx.cs" Inherits="matrakom.contact_us"
    MasterPageFile="~/DefaultMaster.Master" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="ContactUs" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Contact Us</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner">
                                    <div class="right-top-corner">
                                        <div class="right-bot-corner">
                                            <div class="left-bot-corner">
                                                <div class="inner">
                                                    <h3>
                                                        Contact Us</h3>
                                                    <p>
                                                        We are open for whatever questions you may have. Please complete the form below
                                                        to let us know.</p>
                                                    <div id="textContainer">
                                                        <div class="floatLeft">
                                                            <p>
                                                                <strong>Address</strong><br />
                                                                Level 14, Tamara Center<br />
                                                                Jl. Jendral Sudirman 24<br />
                                                                Jakarta Selatan DKI Jakarta Raya 12920
                                                            </p>
                                                        </div>
                                                        <div class="floatRight">
                                                            <p>
                                                                <strong>Phone Number</strong>
                                                                <br />
                                                                T : +62 21 - 252 6660<br />
                                                                F : +62 21 - 252 6655</p>
                                                        </div>
                                                    </div>
                                                    <form class="contacts-form" runat="server">
                                                    <p>
                                                        <asp:Label ID="Label1" runat="server" Visible="false" Font-Bold="true" ForeColor="Red" />
                                                    </p>
                                                    <fieldset>
                                                        <div class="field">
                                                            <label>
                                                                First Name:</label>
                                                            <asp:TextBox ID="FirstName" runat="server" />
                                                        </div>
                                                        <div class="field">
                                                            <label>
                                                                Last Name:</label>
                                                            <asp:TextBox ID="LastName" runat="server" />
                                                        </div>
                                                        <div class="field">
                                                            <label>
                                                                E-mail:</label>
                                                            <asp:TextBox ID="Email" runat="server" />
                                                        </div>
                                                        <div class="field">
                                                            <label>
                                                                Company:</label>
                                                            <asp:TextBox ID="Company" runat="server" />
                                                        </div>
                                                        <div class="field">
                                                            <label>
                                                                Subject:</label>
                                                            <asp:TextBox ID="Subject" runat="server" />
                                                        </div>
                                                        <div class="select">
                                                            <label>
                                                                Send To:</label>
                                                            <asp:DropDownList ID="Receiver" runat="server">
                                                                <asp:ListItem Text="-- Choose Receiver --" Value="0" />
                                                                <asp:ListItem Text=" Sales Division " Value="sales.matrakom@bizintek.com" />
                                                                <asp:ListItem Text=" Support Division " Value="sales.matrakom@bizintek.com" />
                                                                <asp:ListItem Text=" Human Research Division " Value="hrd.matrakom@bizintek.com" />
                                                            </asp:DropDownList>
                                                        </div>
                                                        <div class="message">
                                                            <label style="padding-bottom: 7px">
                                                                Message:</label>
                                                            <asp:TextBox TextMode="MultiLine" ID="Message" runat="server" Width="626px" />
                                                        </div>
                                                        <div class="buttonArea">
                                                            <input type="reset" value="Reset" />
                                                            <asp:Button ID="SendMail" Text="Send" runat="server" OnClick="SendMail_Click" />
                                                        </div>
                                                    </fieldset>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
    </div>
</asp:Content>
