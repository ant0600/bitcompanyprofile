﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="about-bit.aspx.cs" Inherits="matrakom.about_bit"
    MasterPageFile="~/DefaultMaster.Master" %>

<asp:Content ID="AboutBit" ContentPlaceHolderID="MasterPageContent" runat="server">
    <!-- content -->
    <div class="container">
        <!-- main-banner-small begin -->
        <div class="main-banner-small">
            <div class="inner">
                <h1>
                    Business Intelligence Technologies</h1>
            </div>
        </div>
        <!-- main-banner-small end -->
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h2>
                                                        Company Profile</h2>
                                                    <h3>
                                                        About Us</h3>
                                                    <p>
                                                        Is a dynamic and well-established software development and services company with
                                                        substantial experience in providing consulting services for IT and Telecommunications
                                                        Sector in Indonesia (since 2002), Facilitating foreign companies to establish business
                                                        in Indonesia (since 2006). BIT has Establish subsidiaries to enter new business
                                                        areas (since 2009)</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <div class="section">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner maxheight">
                                    <div class="right-top-corner maxheight">
                                        <div class="right-bot-corner maxheight">
                                            <div class="left-bot-corner maxheight">
                                                <div class="inner">
                                                    <h3>
                                                        Our Vision</h3>
                                                    <p>
                                                        Is to be a leading, innovative IT company that delivers high quality IT systems
                                                        and services against competitive costs. Cost effective solution provider<br />
                                                        Value driven organization&#13;
                                                        <br />
                                                    </p>
                                                    <h3>
                                                        Our Mission</h3>
                                                    <p>
                                                        Is providing Business Analytics solutions to major corporations (BIT).<br />
                                                        Seen as a trusted partner for customers as well as business partners – principals,
                                                        resellers and service partners<br />
                                                        Seen as a company that introduces solutions appropriate for the growing Indonesian
                                                        small to medium sector
                                                        <br />
                                                    </p>
                                                    <h3>
                                                        Our Product and Services</h3>
                                                    <p>
                                                        Solution, Quality, and Price are three key elements that affect corporate decisions.
                                                        It is precisely these three characteristics that give our products and services
                                                        the leading edge. The management and consultants at Matrkom possess years of experience
                                                        in providing software and consulting services for the Indonesian market and are
                                                        well versed in selling, implementing and maintaining software applications.
                                                        <br />
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
        <!-- box begin
    <div class="box">
      <div class="border-top">
        <div class="border-right">
          <div class="border-bot">
            <div class="border-left">
              <div class="left-top-corner">
                <div class="right-top-corner">
                  <div class="right-bot-corner">
                    <div class="left-bot-corner">
                      <div class="inner">
                        <h2>Our Team</h2>
                        <ul class="list2">
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>John Doe</strong> President and a big boss</h4>
                            The presidents always come first, but we’re sure you knew that. So here it is - the info about the biggest man on your company. </li>
                          <li> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Sam Cohen</strong> Vice president</h4>
                            Also some kind of info about how good he is at whatever he does - that’s what vice presidents are for. </li>
                          <li class="last"> <img alt="" src="images/image_87x87.gif" />
                            <h4><strong>Jane Doe</strong> Personal assistant</h4>
                            This nice lady would love if you say a couple of good words about her on this page. Besides, it’s not a big deal for you - than why don’t you be nice and just put the info. </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
        
    </div> -->
        <!-- box end -->
    </div>
    <!-- extra-content -->
    <div id="extra-content">
        <div class="container">
            <!-- box begin -->
            <div class="box">
                <div class="border-top">
                    <div class="border-right">
                        <div class="border-bot">
                            <div class="border-left">
                                <div class="left-top-corner">
                                    <div class="right-top-corner">
                                        <div class="right-bot-corner">
                                            <div class="left-bot-corner">
                                                <div class="inner">
                                                    <h3>
                                                        Partners</h3>
                                                    <div class="carousel">
                                                        <ul>
                                                            <!--<li class="prev"><img alt="" src="images/prev.gif" /></li> -->
                                                            <li></li>
                                                            <li></li>
                                                            <li></li>
                                                            <li></li>
                                                            <!--<li class="next"><img alt="" src="images/next.gif" /></li>-->
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- box end -->
        </div>
    </div>
</asp:Content>
